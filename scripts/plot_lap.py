#!/usr/bin/env python3

import sys
import matplotlib.pylab as plt


def load_points(fname):
    points = []
    for line in open(fname).readlines():
        points.append([float(v) for v in line.split(",")])
    return points

def load_assignments(fname):
    assignments = []
    exemplar_map = {}

    for line in open(fname).readlines():
        data = [int(v) for v in line.split(",")]
        if data[1] not in exemplar_map:
            exemplar_map[data[1]] = len(exemplar_map)
        assignments.append(exemplar_map[data[1]])

    return assignments
    
def plot(points, assignments):
    plt.figure(1)
    plt.scatter(
            [v[0] for v in points],
            [v[1] for v in points],
            c=assignments,
            cmap=plt.get_cmap("Set1")
    )
    plt.title("Dimension 1 and 2")
    plt.xlabel("Dimension 1")
    plt.ylabel("Dimension 2")

    plt.figure(2)
    plt.scatter(
            [v[2] for v in points],
            [v[3] for v in points],
            c=assignments,
            cmap=plt.get_cmap("Set1")
    )
    plt.title("Dimension 3 and 4")
    plt.xlabel("Dimension 3")
    plt.ylabel("Dimensin 4")

    plt.show()


def main():
    points = load_points(sys.argv[1])
    assignments = load_assignments(sys.argv[2])
    print(assignments)
    plot(points, assignments)


if __name__ == "__main__":
    sys.exit(main())
