Overview
========

This code provides an implementation of Affinity Propagation [1] and
Layered Affinity Propagation [2].

[1] Frey, B.J. and Dueck, D., 2007. Clustering by passing messages between data points. science  
[2] Ott, L. and Ramos, F., 2013. Multi-Sensor Clustering using Layered Affinity Propagation. In IEEE/RSJ International Conference on Intelligent Robots and Systems

Requirements
============

To build the code the following libraries are required:

- Boost
- Eigen3


Building
========

The code is built using CMake which allows building the code using the following steps.

```
# Create a build folder
cd source_folder
mkdir build
# Configure using cmake
cmake -DCMAKE_BUILD_TYPE=Release ..
# Build the code
make
```

After the compilation finishes there should be two executable in the `source_folder/bin` folder:

- ex_ap
- ex_flo

Both use a simple multi dimensional dataset of several Gaussian
distributed points and cluster them using either affinity propagation or
layered affinity propagation. The layered affinity propagation version
produces output which can be visualized using the provided Python script
as follows:
```
python scripts/plot_lap.py /tmp/lap_points.csv /tmp/lap_points.csv
```


Using Layered Affinity Propagation
==================================

Layered Affinity Propagation takes as input a vector of similarity
matrices, where each matrix corresponds to the similarity metric used
for a particular data source. The other options are identical to
standard affinity propagation.
