// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <iostream>
#include <fstream>

#include <Eigen/Core>

#include <apc/layered_affinity_propagation.h>
#include <apc/params.h>
#include <apc/median_helper.h>

#include "gaussian_dataset.h"


using namespace apc;


void dump_points(
        std::vector<Eigen::VectorXd> const& points,
        std::string const&              fname
)
{
    std::ofstream out(fname);

    for(auto const& pt : points)
    {
        for(size_t i=0; i<pt.size()-1; ++i)
        {
            out << pt[i] << ",";
        }
        out << pt[pt.size()-1] << "\n";
    }
}

void dump_solution(
        std::vector<int> const&         exemplar_ids,
        std::vector<int> const&         assignments,
        std::string const&              fname
)
{
    std::ofstream out(fname);

    for(size_t i=0; i<assignments.size(); ++i)
    {
        out << i << "," << assignments[i] << "\n";
    }
}


int main(int argc, const char *argv[])
{
    // Create dataset
    auto ds = GaussianDataset(10, 4);
    ds.generate_random(1000);
    auto points = ds.points();
    dump_points(points, "/tmp/lap_points.csv");

    Eigen::MatrixXd simmat_1 = Eigen::MatrixXd(points.size(), points.size());
    Eigen::MatrixXd simmat_2 = Eigen::MatrixXd(points.size(), points.size());

    MedianHelper mh(2.0, 0.05);
    for(size_t i=0; i<points.size(); ++i)
    {
        for(size_t j=0; j<points.size(); ++j)
        {
            if(i != j)
            {
                simmat_1(i, j) = -(points[i].head(2) - points[j].head(2)).norm();
                simmat_2(i, j) = -(points[i].tail(2) - points[j]).tail(2).norm();
                mh.add(simmat_1(i, j));
                mh.add(simmat_2(i, j));
            }
        }
    }
    for(size_t i=0; i<points.size(); ++i)
    {
        simmat_1(i, i) = mh.value();
        simmat_2(i, i) = mh.value();
    }


    // Cluster
    std::vector<Eigen::MatrixXd> sim_mats;
    sim_mats.push_back(simmat_1);
    sim_mats.push_back(simmat_2);
    LayeredAffinityPropagation lap(sim_mats, default_params());
    int iters = lap.iterate(250, 20);

    // Show results
    double score = lap.score();
    auto exemplar_ids = lap.exemplar_ids();
    auto assignments = lap.assignments();
    std::cout << "score=" << score
              << " clusters=" << exemplar_ids.size()
              << " iters=" << iters
              << std::endl;

    dump_solution(exemplar_ids, assignments, "/tmp/lap_solution.csv");

    return 0;
}

