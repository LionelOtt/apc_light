// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef __GAUSSIAN_DATASET_H__
#define __GAUSSIAN_DATASET_H__


#include <cmath>
#include <numeric>
#include <random>

#include <Eigen/Cholesky>
#include <Eigen/LU>


/**
 * \brief Generates datasets containing Gaussian distributed data.
 */
class GaussianDataset
{
    typedef std::mt19937 Generator_t;
    typedef std::uniform_real_distribution<> UniformDist_t;
    typedef std::normal_distribution<> NormalDist_t;

    public:
        /**
         * \brief Creates a new Gaussian dataset.
         *
         * \param clusters the number of Gaussian clusters to generate
         * \param dimensions the number of dimensions of the data
         * \param seed the seed for the random number generator
         */
        GaussianDataset(int clusters, int dimensions, int seed=0);

        void generate_uniform(int points);
        void generate_random(int points);
        void add_noise(double percent);

        std::vector<Eigen::VectorXd> const& points() const;
        std::vector<Eigen::VectorXd> points();

        std::vector<Eigen::VectorXd> centers() const;

    private:
        void create_model();

        std::vector<Eigen::VectorXd> sample_from_gaussian(
                Eigen::VectorXd const&  mu,
                Eigen::MatrixXd const&  sigma,
                int                     count
        );

    private:
        std::vector<Eigen::VectorXd>    m_points;
        std::vector<int>                m_ground_truth;
        int                             m_clusters;
        int                             m_dimensions;

        // Model stuff
        Generator_t                     m_generator;
        NormalDist_t                    m_normal_dist;
        std::vector<Eigen::MatrixXd>    m_sigma;
        std::vector<Eigen::VectorXd>    m_mu;
};


#endif /* __GAUSSIAN_DATASET_H__ */
