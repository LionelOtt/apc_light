// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <iostream>

#include <Eigen/Core>

#include <apc/affinity_propagation.h>
#include <apc/params.h>
#include <apc/median_helper.h>

#include "gaussian_dataset.h"


using namespace apc;


int main(int argc, const char *argv[])
{
    // Create dataset
    auto ds = GaussianDataset(8, 5);
    ds.generate_random(3000);
    auto points = ds.points();
    Eigen::MatrixXd simmat = Eigen::MatrixXd(points.size(), points.size());
    MedianHelper mh(5.0, 0.05);
    for(size_t i=0; i<points.size(); ++i)
    {
        for(size_t j=0; j<points.size(); ++j)
        {
            if(i != j)
            {
                simmat(i, j) = -(points[i] - points[j]).norm();
                mh.add(simmat(i, j));
            }
        }
    }
    for(size_t i=0; i<points.size(); ++i)
    {
        simmat(i, i) = mh.value();
    }


    // Cluster
    AffinityPropagation ap(simmat, default_params());
    int iters = ap.iterate(250, 20);

    // Show results
    double score = ap.score();
    auto exemplar_ids = ap.exemplar_ids();
    auto assignments = ap.assignments();
    std::cout << "score=" << score
              << " clusters=" << exemplar_ids.size()
              << " iters=" << iters
              << std::endl;

    return 0;
}
