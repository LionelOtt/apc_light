// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <limits>

#include <apc/ap_layer.h>
#include <apc/ap_merge_layer.h>


namespace apc
{


APMergeLayer::APMergeLayer(int dim, size_t count, double damping_factor)
    :   damping(damping_factor)
      , dim(dim)
      , count(count)
      , phi(count, Eigen::MatrixXd::Zero(dim, dim))
      , tau(count, Eigen::MatrixXd::Zero(dim, dim))
      , initialized(false)
{}

void APMergeLayer::update(std::vector<APLayer> const& layers)
{
    if(!initialized)
    {
        sim_sum = Eigen::MatrixXd::Zero(dim, dim);
        for(auto const& layer : layers)
        {
            initialized = true;
            sim_sum += layer.sim;
        }
    }

    compute_tau(layers);
    compute_phi(layers);
}

void APMergeLayer::compute_phi(std::vector<APLayer> const& layers)
{
    for(size_t id=0; id<phi.size(); ++id)
    {
        // Compute max message values
        Eigen::MatrixXd tau_max = tau[id].cwiseMax(Eigen::MatrixXd::Zero(dim, dim));
        tau_max.diagonal() = tau[id].diagonal();
        Eigen::MatrixXd rho_max = layers[id].rho.cwiseMax(Eigen::MatrixXd::Zero(dim, dim));
        rho_max.diagonal() = layers[id].rho.diagonal();

        // Compute message sums
        Eigen::VectorXd tau_max_sum = tau_max.colwise().sum();
        Eigen::VectorXd rho_max_sum = tau_max.colwise().sum();

        // Create new phi matrix
        Eigen::MatrixXd new_phi = Eigen::MatrixXd::Zero(dim, dim);
        new_phi.rowwise() = (tau_max_sum + rho_max_sum).transpose();
        new_phi -= tau_max;

        // Perform min opertion on non-diagonal elements
        Eigen::VectorXd new_phi_diag(new_phi.diagonal());
        new_phi = new_phi.cwiseMin(Eigen::MatrixXd::Zero(dim, dim));
        new_phi.diagonal() = new_phi_diag;

        // Update phi with damping
        phi[id] = (damping * phi[id]) + ((1.0 - damping) * new_phi);
    }
}

void APMergeLayer::compute_tau(std::vector<APLayer> const& layers)
{
    Eigen::MatrixXd sim_phi_sum(sim_sum);
    Eigen::MatrixXd phi_sum = Eigen::MatrixXd::Zero(dim, dim);
    for(auto const& mat : phi)
    {
        sim_phi_sum += mat;
        phi_sum += mat;
    }

    for(size_t id=0; id<tau.size(); ++id)
    {
        // Compute values for one row at a time
        Eigen::MatrixXd tmp = Eigen::MatrixXd::Zero(dim, dim);
        for(int i=0; i<dim; ++i)
        {
            Eigen::MatrixXd::Index idx;
            Eigen::VectorXd vec = sim_phi_sum.row(i);

            // Set max value for all but the max index
            double value = vec.maxCoeff(&idx);
            tmp.row(i).fill(value);
            // Set max value for the max value
            vec[idx] = -std::numeric_limits<double>::max();
            tmp(i, idx) = vec.maxCoeff();
        }


        tau[id] = (damping * tau[id]) +
            ((1.0 - damping) * (sim_sum - tmp + (phi_sum - phi[id])));

    }
}


} /* apc */
