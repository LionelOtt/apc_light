// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <apc/abstract_affinity_propagation.h>


namespace apc
{


AbstractAffinityPropagation::AbstractAffinityPropagation(
        Parameters const&       params
)
    :   m_params(params)
      , m_hard_assignments_valid(false)
      , m_prob_assignments_valid(false)
{
    m_scores.push_back(0.0);
}

AbstractAffinityPropagation::~AbstractAffinityPropagation()
{}

int AbstractAffinityPropagation::iterate(int max_iters, int conv_iters)
{
    m_hard_assignments_valid = false;
    m_prob_assignments_valid = false;
    m_scores.clear();
    m_scores.push_back(0.0);

    return do_iterate(max_iters, conv_iters);
}

double AbstractAffinityPropagation::score() const
{
    return m_scores.back();
}

std::vector<int> & AbstractAffinityPropagation::exemplar_ids()
{
    if(!m_hard_assignments_valid)
    {
        compute_hard_assignments();
        m_hard_assignments_valid = true;
    }

    return m_exemplar_ids;
}

std::vector<int> & AbstractAffinityPropagation::assignments()
{
    if(!m_hard_assignments_valid)
    {
        compute_hard_assignments();
        m_hard_assignments_valid = true;
    }

    return m_assignments;
}

std::vector<std::vector<double>> &
AbstractAffinityPropagation::prob_assignments()
{
    if(!m_prob_assignments_valid)
    {
        compute_prob_assignments();
        m_prob_assignments_valid = true;
    }

    return m_prob_assignments;
}

std::vector<double> AbstractAffinityPropagation::scores() const
{
    return m_scores;
}

void AbstractAffinityPropagation::set_params(Parameters const& params)
{
    m_params = params;
    do_set_params();
    clear();
}

Parameters AbstractAffinityPropagation::get_params() const
{
    return m_params;
}

void AbstractAffinityPropagation::clear()
{
    do_clear();

    m_hard_assignments_valid = false;
    m_prob_assignments_valid = false;
    m_scores.clear();
    m_exemplar_ids.clear();
    m_assignments.clear();
    m_prob_assignments.clear();
}


} /* apc */
