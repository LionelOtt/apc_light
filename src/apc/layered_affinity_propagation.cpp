// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <apc/layered_affinity_propagation.h>


namespace apc
{


LayeredAffinityPropagation::LayeredAffinityPropagation(
        std::vector<Eigen::MatrixXd>    similarities,
        Parameters const&               params
)   :   AbstractAffinityPropagation(params)
{
    // Create layer AP instances
    for(size_t i=0; i<similarities.size(); ++i)
    {
        assert(similarities[i].maxCoeff() <= 0.0);

        m_layers.push_back(APLayer(similarities[i], m_params.get<double>("damping"), i));
    }
    m_merge = APMergeLayer(similarities[0].rows(), similarities.size(), 0.9);

    m_layer_scores.resize(similarities.size());
}

std::vector<std::vector<double>> LayeredAffinityPropagation::layer_scores() const
{
    return m_layer_scores;
}

int LayeredAffinityPropagation::do_iterate(int max_iters, int conv_iters)
{
    double old_score = 0.0;
    int conv_count = 0;
    int iterations = 0;

    for(int i=0; i<max_iters; ++i)
    {
        // Update all messages
        for(auto & layer : m_layers)
        {
            layer.update(m_merge);
        }
        m_merge.update(m_layers);

        // Check for convergence
        compute_hard_assignments();

        // Allow the score to vary a bit around the current one but do not
        // update it because otherwise we could change the score over a
        // period of time
        if(std::abs(old_score - m_scores.back()) <
           (m_params.get<double>("convergence_tolerance") * std::abs(m_scores.back()))
        )
        {
            conv_count++;
        }
        else
        {
            conv_count = 0;
            old_score = m_scores.back();
        }
        iterations++;
        if(conv_count >= conv_iters)
        {
            break;
        }
    }

    return iterations;
}

void LayeredAffinityPropagation::compute_hard_assignments()
{
    // Reset state storage
    double score = 0.0;
    m_exemplar_ids.clear();
    m_assignments.clear();
    m_assignments.reserve(m_layers[0].dim);

    int dim = m_layers[0].dim;

    // Compute commulative alpha and rho matrix
    Eigen::MatrixXd alpha = Eigen::MatrixXd::Zero(dim, dim);
    Eigen::MatrixXd rho = Eigen::MatrixXd::Zero(dim, dim);
    for(size_t i=0; i<m_layers.size(); ++i)
    {
        alpha += m_merge.phi[i];
        rho += m_merge.tau[i];
    }

    // Determine exemplars
    auto indicator = (alpha + rho).diagonal();
    for(int i=0; i<dim; ++i)
    {
        if(indicator(i) > 0.0)
        {
            m_exemplar_ids.push_back(i);
        }
    }

    // Return if no exemplars have been identified so far
    if(m_exemplar_ids.empty())
    {
        return;
    }

    // Create matrix with exemplar to data point scores
    Eigen::MatrixXd exemplar_scores(dim, m_exemplar_ids.size());
    for(size_t i=0; i<m_exemplar_ids.size(); ++i)
    {
        int id = m_exemplar_ids[i];
        exemplar_scores.col(i) = alpha.col(id) + rho.col(id);
    }
    // Determine exemplars for every data point
    for(int i=0; i<dim; ++i)
    {
        Eigen::MatrixXd::Index index;
        exemplar_scores.row(i).maxCoeff(&index);
        int id = m_exemplar_ids[index];
        m_assignments.push_back(id);

        if(i != id)
        {
            //score += alpha(i, id) + rho(i, id);
            score += m_merge.sim_sum(i, id);
        }
    }
    // Fix exemplar self assignments
    for(size_t i=0; i<m_exemplar_ids.size(); ++i)
    {
        int id = m_exemplar_ids[i];
        m_assignments[id] = m_exemplar_ids[i];
        score += m_merge.sim_sum(id, id);
    }

    m_scores.push_back(score / m_assignments.size());

    // Compute scores for each individual layer
    for(size_t i=0; i<m_layers.size(); ++i)
    {
        m_layer_scores[i].push_back(m_layers[i].score());
    }
}

void LayeredAffinityPropagation::compute_prob_assignments()
{}

void LayeredAffinityPropagation::do_clear()
{
    for(auto & layer : m_layers)
    {
        layer.alpha.fill(0.0);
        layer.rho.fill(0.0);
    }
    for(size_t i=0; i<m_merge.phi.size(); ++i)
    {
        m_merge.phi[i].fill(0.0);
        m_merge.tau[i].fill(0.0);
    }

    m_layer_scores.clear();
}

void LayeredAffinityPropagation::do_set_params()
{
    for(auto & layer : m_layers)
    {
        layer.damping = m_params.get<double>("damping");
    }
    m_merge.damping = m_params.get<double>("damping");
}


} /* apc */
