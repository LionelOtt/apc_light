// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <iostream>
#include <limits>

#include <apc/affinity_propagation.h>
#include <apc/util.h>


namespace apc
{


AffinityPropagation::AffinityPropagation(
        Eigen::MatrixXd const&          similarities,
        Parameters const&       params
)
    :   AbstractAffinityPropagation(params)
      , m_similarities(similarities)
      , m_availabilities(Eigen::MatrixXd::Zero(similarities.rows(), similarities.cols()))
      , m_responsabilities(Eigen::MatrixXd::Zero(similarities.rows(), similarities.cols()))
{}

int AffinityPropagation::do_iterate(int max_iters, int conv_iters)
{
    double old_score = 0.0;
    int conv_count = 0;
    int iterations = 0;

    for(int i=0; i<max_iters; ++i)
    {
        update_responsabilities();
        update_availabilities();

        // Check for convergence
        compute_hard_assignments();

        // Allow the score to vary a bit around the current one but do not
        // update it because otherwise we could change the score over a
        // period of time
        if(std::abs(old_score - m_scores.back()) <
           (m_params.get<double>("convergence_tolerance") * std::abs(m_scores.back()))
        )
        {
            conv_count++;
        }
        else
        {
            conv_count = 0;
            old_score = m_scores.back();
        }
        iterations++;
        if(conv_count >= conv_iters)
        {
            break;
        }
    }

    return iterations;
}

void AffinityPropagation::compute_hard_assignments()
{
    // Reset state storage
    double score = 0.0;
    m_exemplar_ids.clear();
    m_assignments.clear();
    m_assignments.reserve(m_similarities.rows());

    // Determine exemplars
    Eigen::VectorXd diag = (m_availabilities.diagonal() + m_responsabilities.diagonal());
    for(int i=0; i<diag.size(); ++i)
    {
        if(diag(i) > 0.0)
        {
            m_exemplar_ids.push_back(i);
        }
    }

    // Return if no exemplars have been identified so far
    if(m_exemplar_ids.empty())
    {
        m_scores.push_back(0.0);
        return;
    }

    // Create matrix with exemplar to data point scores
    Eigen::MatrixXd exemplar_scores(m_similarities.rows(), m_exemplar_ids.size());
    for(size_t i=0; i<m_exemplar_ids.size(); ++i)
    {
        int id = m_exemplar_ids[i];
        exemplar_scores.col(i) = m_availabilities.col(id) + m_responsabilities.col(id);
    }
    // Determine exemplars for every data point
    for(int i=0; i<m_similarities.rows(); ++i)
    {
        int index;
        exemplar_scores.row(i).maxCoeff(&index);
        int id = m_exemplar_ids[index];
        m_assignments.push_back(id);

        if(i != id)
        {
            score += m_similarities(i, id);
        }
    }
    // Fix exemplar self assignments
    for(size_t i=0; i<m_exemplar_ids.size(); ++i)
    {
        m_assignments[m_exemplar_ids[i]] = m_exemplar_ids[i];
        score += m_similarities(m_exemplar_ids[i], m_exemplar_ids[i]);
    }

    m_scores.push_back(score / m_assignments.size());
}

void AffinityPropagation::compute_prob_assignments()
{
    compute_hard_assignments();

    int count = m_similarities.rows();

    // Compute the probabilistic assignments
    m_prob_assignments.clear();
    m_prob_assignments.reserve(count);

    // For each data point compute the assignment probability to each of
    // the exemplars.
    for(int i=0; i<count; ++i)
    {
        // Probability storage
        std::vector<double> prob(m_exemplar_ids.size());
        double normalizer = 0.0;

        // Compute the exponential value for each element
        for(size_t j=0; j<m_exemplar_ids.size(); ++j)
        {
            prob[j] = logistic(
                    m_availabilities(i, j) + m_responsabilities(i, j)
            );
            normalizer += prob[j];
        }
        // Normalize the values to obtain proper probailities
        for(size_t j=0; j<prob.size(); ++j)
        {
            prob[j] = prob[j] / normalizer;
        }
        m_prob_assignments.push_back(prob);
    }
}

void AffinityPropagation::do_clear()
{
    m_availabilities.fill(0.0);
    m_responsabilities.fill(0.0);
}

void AffinityPropagation::do_set_params()
{}

void AffinityPropagation::update_responsabilities()
{
    int N = m_similarities.rows();

    // Compute matrix of row maximas
    Eigen::MatrixXd AS = m_availabilities + m_similarities;

    Eigen::MatrixXd M = Eigen::MatrixXd::Zero(N, N);

    // Compute max(a(i, k') + s(i, k'))
    // Ignore the k' and simply set all entries to the row maxima and the
    // entry whose value is the maxima is set to the second maxima in the row
    for(int i=0; i<N; ++i)
    {
        int max_index = 0;
        double max_value = AS.row(i).maxCoeff(&max_index);
        M.row(i) = Eigen::MatrixXd::Constant(1, N, max_value);
        AS(i, max_index) = -std::numeric_limits<double>::max();
        M(i, max_index) = AS.row(i).maxCoeff();
    }

    // Update and dampen values
    m_responsabilities =
        (1.0 - m_params.get<double>("damping")) * (m_similarities - M) +
        m_params.get<double>("damping") * m_responsabilities;
}

void AffinityPropagation::update_availabilities()
{
    int N = m_similarities.rows();

    //MatrixXd R0 = m_responsabilities.cwise().max(MatrixXd::Zero(N, N));
    Eigen::MatrixXd R0 = m_responsabilities.array().cwiseMax(
            Eigen::MatrixXd::Zero(N, N).array()
    );
    
    // Handle i' == k
    R0.diagonal() = m_responsabilities.diagonal();

    // Create matrix with raw column sums disregarding the constraint
    // i' != i and i' != k
    Eigen::MatrixXd T = Eigen::MatrixXd::Zero(N, N);
    Eigen::VectorXd sum_vec = R0.colwise().sum();
    for(int i=0; i<N; ++i)
    {
        T.row(i) = sum_vec;
    }

    // Handle i' == i
    T -= R0;

    // Compute final matrix and handle a(k, k) case by storing the values
    // on the diagonal and reassigning them after the computation
    Eigen::MatrixXd T_diag = T.diagonal();
    //T = T.cwise().min(MatrixXd::Zero(N, N));
    T = T.array().cwiseMin(Eigen::MatrixXd::Zero(N, N).array());
    T.diagonal() = T_diag;

    // Update and dampen
    m_availabilities =
        (1.0 - m_params.get<double>("damping")) * T +
        m_params.get<double>("damping") * m_availabilities;
}


} /* apc */
