// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef __APC_AP_MERGE_LAYER_H__
#define __APC_AP_MERGE_LAYER_H__


#include <vector>

#include <Eigen/Core>


namespace apc
{


struct APLayer;

class APMergeLayer
{
    public:
        double                          damping;
        int                             dim;
        size_t                          count;

        // Messages
        std::vector<Eigen::MatrixXd>    phi;
        std::vector<Eigen::MatrixXd>    tau;

        Eigen::MatrixXd                 sim_sum;
        bool                            initialized;

    public:
        APMergeLayer() = default;
        APMergeLayer(int dim, size_t count, double damping_factor);

        void update(std::vector<APLayer> const& layers);

    private:
        void compute_phi(std::vector<APLayer> const& layers);
        void compute_tau(std::vector<APLayer> const& layers);
};


} /* apc */


#endif /* __APC_AP_MERGE_LAYER_H__ */
