// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef __APC_MEDIAN_HELPER_H__
#define __APC_MEDIAN_HELPER_H__


#include <vector>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/median.hpp>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>

#include <boost/shared_ptr.hpp>


namespace apc
{


/**
 * \brief Generates random data around the median of accumulated data.
 *
 * This class can be used to compute the median of a collection of affinity
 * propagation data point similarities and at the end obtain slightly
 * varied median values for use as the self preference penalty.
 */
class MedianHelper
{
    typedef boost::mt19937 Generator_t;
    typedef boost::uniform_real<> Distribution_t;
    typedef boost::variate_generator<Generator_t&, Distribution_t> ValueGenerator_t;
    typedef boost::shared_ptr<ValueGenerator_t> ValueGeneratorPtr_t;

    public:
        /**
         * \brief Creates a new instance with a set amount of variance.
         *
         * The variance is used when generating values from the correct
         * median value. The scaling factor is used to increase the value
         * of the computed median.
         *
         * \param scale the scaling factor for the median value
         * \param variance the variance of the generated values
         */
        MedianHelper(double scale=1.0, double variance=0.01);

        /**
         * \brief Adds a single value to the accumulator.
         *
         * \param value the value to add
         */
        void add(double value);

        /**
         * \brief Adds all values contained in the vector into the accumulator.
         *
         * \param values vector containing values to be added
         */
        void add(std::vector<double> const& values);

        /**
         * \brief Returns a value with some deviation from the median.
         *
         * \return value with some variation aroun the current median
         */
        double value();

        /**
         * \brief Sets the distribution parameters.
         *
         * \param scale scaling factor for the generated values
         * \param variance the variance of the generated random values
         */
        void set_params(double scale, double variance);

    private:
        /**
         * \brief Initializes the random number generator with the current state.
         *
         * Uses the information from the accumulator to create a random number
         * generator.
         */
        void init_rng();

    private:
        //! Value accumulator
        boost::accumulators::accumulator_set<
            double, boost::accumulators::stats<
                boost::accumulators::tag::median(
                        boost::accumulators::with_p_square_quantile)
                > > m_accumulator;

        //! Indicates if the random number generator is ready for use
        bool                            m_rng_ready;
        //! Scaling factor for the median value
        double                          m_scale;
        //! Variance for the generated data points
        double                          m_variance;

        //! Random number generator
        Generator_t                     m_rng_generator;
        //! Random number distribution
        Distribution_t                  m_rng_distribution;
        //! Random number value generator
        ValueGeneratorPtr_t             m_rng;
};


inline
void MedianHelper::add(double value)
{
    m_accumulator(value);
    m_rng_ready = false;
}

inline
void MedianHelper::add(std::vector<double> const& values)
{
    std::for_each(values.begin(), values.end(), m_accumulator);
    m_rng_ready = false;
}

inline
double MedianHelper::value()
{
    if(!m_rng_ready)
    {
        init_rng();
    }
    return (*m_rng)();
}


} /* apc */


#endif /* __APC_MEDIAN_HELPER_H__ */
