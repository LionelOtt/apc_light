// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef __APC_LAYERED_AFFINITY_PROPAGATION_H__
#define __APC_LAYERED_AFFINITY_PROPAGATION_H__


#include <apc/abstract_affinity_propagation.h>
#include <apc/ap_layer.h>
#include <apc/ap_merge_layer.h>
#include <apc/params.h>


namespace apc
{


class LayeredAffinityPropagation : public AbstractAffinityPropagation
{
    public:
        LayeredAffinityPropagation(
                std::vector<Eigen::MatrixXd> similarities,
                Parameters const& params
        );

        std::vector<std::vector<double>> layer_scores() const;

    private:
        virtual int do_iterate(int max_iters, int conv_iters);
        virtual void compute_hard_assignments();
        virtual void compute_prob_assignments();
        virtual void do_clear();
        virtual void do_set_params();

    private:
        std::vector<APLayer>            m_layers;
        APMergeLayer                    m_merge;

        std::vector<std::vector<double>> m_layer_scores;
};


} /* apc */


#endif /* __APC_LAYERED_AFFINITY_PROPAGATION_H__ */
