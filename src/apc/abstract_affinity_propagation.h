// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef __APC_ABSTRACT_AFFINITY_PROPAGATION_H__
#define __APC_ABSTRACT_AFFINITY_PROPAGATION_H__


#include <vector>

#include <apc/parameters.h>


namespace apc
{


/**
 * \brief Defines the interface for affinity propagation implementations.
 *
 * This abstract base class defines the public interface of all affinity
 * propagation implementations.
 */
class AbstractAffinityPropagation
{
    public:
        /**
         * \brief Creates a new instance.
         *
         * \param params parameter set to use
         */
        AbstractAffinityPropagation(Parameters const& params);

        /**
         * \brief Default destructor.
         */
        virtual ~AbstractAffinityPropagation();

        /**
         * \brief Runs the affinity propagation steps until convergence.
         *
         * \param max_iters maximum number of iterations to run
         * \param conv_iters number of iterations the exemplars have to stay
         *      the same for convergence to be achieved
         * \return the number of iterations performed
         */
        int iterate(int max_iters, int conv_iters);

        /**
         * \brief Returns the score of the current clustering solution.
         *
         * \return score of the current clustering solution
         */
        double score() const;

        /**
         * \brief Returns the ids of data points selected as exemplars.
         *
         * \return id of exemplars
         */
        std::vector<int> & exemplar_ids();

        /**
         * \brief Returns the assignment of each data point to an exemplar.
         *
         * The ids in the assignments list refer to the actual point id and
         * not the index into the exemplar_ids. That is the values stored
         * in exemplar_ids are the only valid values to appear inside
         * the assignments list.
         *
         * \return data point to exemplar assignments
         */
        std::vector<int> & assignments();

        /**
         * \brief Returns the PDF of data point to exemplar assignments.
         *
         * The ids in the assignments list refer to the actual point id and
         * not the index into the exemplar_ids. That is the values stored
         * in exemplar_ids are the only valid values to appear inside
         * the assignments list.
         *
         * \return pdf of data point to exemplar assignments
         */
        std::vector<std::vector<double>> & prob_assignments();

        /**
         * \brief Returns the the score history of the last run.
         *
         * \return the score history of the last run
         */
        std::vector<double> scores() const;

        /**
         * \brief Sets the parameters to be used with affinity propagation.
         *
         * \param params new set of parameters
         */
        void set_params(Parameters const& params);

        /**
         * \brief Returns the current parameter set.
         *
         * \return current parameter set
         */
        Parameters get_params() const;

    private:
        /**
         * \brief Runs the affinity propagation steps until convergence.
         *
         * \param max_iters maximum number of iterations to run
         * \param conv_iters number of iterations the exemplars have to stay
         *      the same for convergence to be achieved
         * \return the number of iterations performed
         */
        virtual int do_iterate(int max_iters, int conv_iters) = 0;

        /**
         * \brief Computes the hard assignment of data points to exemplars.
         */
        virtual void compute_hard_assignments() = 0;

        /**
         * \brief Computes the pdf of data point assignments to exemplars.
         */
        virtual void compute_prob_assignments() = 0;

        /**
         * \brief Clears any results accumulated by the method.
         *
         * This method is run when the parameter set is changed in order
         * to avoid stale data to corrupt future computations.
         */
        virtual void do_clear() = 0;

        /**
         * \brief Called when the parameter set is changed.
         *
         * Allows subclasses to react to changes of the parameter set.
         */
        virtual void do_set_params() = 0;

        /**
         * \brief Clears any results accumulated by the method.
         *
         * This method is run when the parameter set is changed in order
         * to avoid stale data to corrupt future computations.
         *
         * This method calls AbstractAffinityPropagation::do_clear to allow
         * subclasses to clean implementation specific details.
         */
        void clear();

    protected:
        //! Contains the ids of the selected exemplars
        std::vector<int>                m_exemplar_ids;
        //! Assignment of data points to exemplars
        std::vector<int>                m_assignments;
        //! Assignment PDF of data points
        std::vector<std::vector<double>> m_prob_assignments;
        //! History of all the scores
        std::vector<double>             m_scores;

        //! Parameters used by affinity propagation
        Parameters                      m_params;

        //! Flag indicating if the hard assignments are valid
        bool                            m_hard_assignments_valid;
        //! Flag indicating if the probabilistic assignments are valid
        bool                            m_prob_assignments_valid;
};


} /* apc */


#endif /* __APC_ABSTRACT_AFFINITY_PROPAGATION_H__ */
