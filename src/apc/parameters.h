// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#ifndef __APC_PARAMETERS_H__
#define __APC_PARAMETERS_H__


#include <map>
#include <string>
#include <vector>

#include <boost/any.hpp>

#include <apc/exception.h>


namespace apc
{


/**
 * \brief Stores values associated with keys of arbitrary data types.
 */
class Parameters
{
    public:
        /**
         * \brief Enumeration of valid data types.
         */
        enum class ValueType
        {
            Bool,
            Int,
            Float,
            Double,
            String
        };

    public:
        /**
         * \brief Sets the value of a key to the provided value.
         *
         * After this method returns the entry with name key will have the
         * provided value. If an entry with the given key already exists
         * its value will be changed. If no entry exists for the key a new
         * entry will be created.
         *
         * \param key name of the key to set
         * \param value the new value for the provided key
         */
        void set(std::string const& key, boost::any value);

        /**
         * \brief Returns the value associated with the provided key.
         *
         * If no entry exists for the provided key a ParameterError exception
         * will be thrown.
         *
         * \param key name of the entry to return the value of
         * \return value associated with the provided key
         */
        template<typename T>
        T get(std::string const& key) const;

        /**
         * \brief Clears the underlying storage.
         */
        void clear();

        /**
         * \brief Returns an instance with the entries of the two instances.
         *
         * If both instances contain an entry with the same key the value of
         * the instance on which merge was called is preserved.
         *
         * \param other the instance with which to merge this one
         * \return new instance with the values of both provided instances
         */
        Parameters merge(Parameters const& other);

        /**
         * \brief Reads an ini file and populates the storage with it's values.
         *
         * The keys parameter contains the list of key names and their
         * data type of the associated data.
         *
         * \param fname name of the ini file to parse
         * \param keys the keys to read from the provided ini file
         */
        void read_ini_file(
            std::string const&          fname,
            std::vector<std::pair<std::string, ValueType>> const& keys
        );

        /**
         * \brief Returns all keys of this parameter set.
         *
         * \return all keys present in this parameter set
         */
        std::vector<std::string> keys() const;

    private:
        //! Key, value storage
        std::map<std::string, boost::any> m_params;
};

template<typename T>
T Parameters::get(std::string const& key) const
{
    auto itr = m_params.find(key);
    if(itr == m_params.end())
    {
        throw APException("Invalid key specified");
    }

    try
    {
        return boost::any_cast<T>(itr->second);
    }
    catch(boost::bad_any_cast const& e)
    {
        throw APException("Invalid cast requested");
    }
}


} /* apc */ 

#endif /* __APC_PARAMETERS_H__ */
