// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <apc/params.h>


namespace apc
{

Parameters default_params()
{
    Parameters params;

    // -------------------------------------------------------------------------
    // Global settings

    //! Damping applied to iterations of affinity propagation
    params.set("damping", 0.9);
    //! Scaling factor applied to median values computed using
    //! the MedianHelper class
    params.set("similarity_variance", 0.05);
    //! Noise added to the median values computed using the
    //! MedianHelper class
    params.set("similarity_factor", 5.0);
    //! Amount of jitter in the net similarity allowed between iterations
    //! to still consider an iteration to be itentical
    params.set("convergence_tolerance", 0.05);

    // -------------------------------------------------------------------------
    // STRAP settings

    //! Size of the reservoir for points the current model can't represent
    params.set("reservoir_size", 100);
    //! Size of the time windows, influences the "forgetting" of
    //! unobserved data
    params.set("window_size", 600.0);
    //! Maximal distance to still consider a new point to be representable
    //! by an existing cluster
    params.set("distance_threshold", 1.2);

    // -------------------------------------------------------------------------
    // Sparse AP settings

    //! Distance at which points are considered to be disconnected
    params.set("max_point_distance", 10.0);

    // -------------------------------------------------------------------------
    // MPAP settings

    //! Maximum radius of a meta point
    params.set("max_radius", 1.0);
    //! Minimal number of points a meta point needs to represent for it
    //! to be considered for clustering
    params.set("min_points", 5);
    //! Time in seconds before unobserved noise meta point will be pruned
    //! from the system
    params.set("pruning_window", 60.0);

    // -------------------------------------------------------------------------
    // Layered AP settings

    //! Number of layers to use with layered AP
    params.set("layers", 2);

    // -------------------------------------------------------------------------
    // Outlier AP settings
    params.set("outlier_points", 50);

    // -------------------------------------------------------------------------
    // LSH settings
    params.set("lsh_instances", 50);
    // This needs to be set by the application
    //params.set("lsh_data_dimension", 10);
    params.set("lsh_hash_count", 5);
    params.set("lsh_bin_width", 5.0);

    return params;
}

void load_params(std::string const& fname, Parameters & params)
{
    std::vector<std::pair<std::string, Parameters::ValueType>> keys = {
        {"damping", Parameters::ValueType::Double},
        {"similarity_variance", Parameters::ValueType::Double},
        {"similarity_factor", Parameters::ValueType::Double},
        {"convergence_tolerance", Parameters::ValueType::Double},
        {"reservoir_size", Parameters::ValueType::Int},
        {"window_size", Parameters::ValueType::Double},
        {"distance_threshold", Parameters::ValueType::Double},
        {"max_point_distance", Parameters::ValueType::Double},
        {"max_radius", Parameters::ValueType::Double},
        {"min_points", Parameters::ValueType::Int},
        {"pruning_window", Parameters::ValueType::Double},
        {"layers", Parameters::ValueType::Int},
        {"outlier_points", Parameters::ValueType::Int},
        {"lsh_instances", Parameters::ValueType::Int},
        {"lsh_hash_count", Parameters::ValueType::Int},
        {"lsh_bin_width", Parameters::ValueType::Double}
    };

    params.read_ini_file(fname, keys);
}

} /* apc */
