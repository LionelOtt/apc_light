// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <iostream>
#include <vector>

#include <boost/any.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include <apc/parameters.h>


namespace apc
{

void Parameters::set(std::string const& key, boost::any value)
{
    m_params[key] = value;
}

void Parameters::clear()
{
    m_params.clear();
}

Parameters Parameters::merge(Parameters const& other)
{
    Parameters output;
    output.m_params = m_params;

    for(auto const& entry : other.m_params)
    {
        if(output.m_params.find(entry.first) == output.m_params.end())
        {
            output.m_params.insert(entry);
        }
    }

    return output;
}

void Parameters::read_ini_file(
    std::string const&          fname,
    std::vector<std::pair<std::string, ValueType>> const& keys
)
{
    boost::property_tree::ptree tree;
    boost::property_tree::ini_parser::read_ini(fname, tree);

    for(auto const& key : keys)
    {
        if(key.second == ValueType::Float)
        {
            m_params.insert({key.first, tree.get<float>(key.first)});
        }
        else if(key.second == ValueType::Double)
        {
            m_params.insert({key.first, tree.get<double>(key.first)});
        }
        else if(key.second == ValueType::Int)
        {
            m_params.insert({key.first, tree.get<int>(key.first)});
        }
        else if(key.second == ValueType::Bool)
        {
            m_params.insert({key.first, tree.get<bool>(key.first)});
        }
        else if(key.second == ValueType::String)
        {
            m_params.insert({key.first, tree.get<std::string>(key.first)});
        }
        else
        {
            throw APException("Invalid value type used for entry: " + key.first);
        }
    }
}

std::vector<std::string> Parameters::keys() const
{
    std::vector<std::string> key_names;
    for(auto const& pair : m_params)
    {
        key_names.push_back(pair.first);
    }
    return key_names;
}

} /* apc */ 
