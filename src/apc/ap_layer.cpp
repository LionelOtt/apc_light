// Copyright (c) 2016, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <limits>

#include <apc/ap_layer.h>
#include <apc/ap_merge_layer.h>


namespace apc
{


APLayer::APLayer(
        Eigen::MatrixXd                 similarities,
        double                          damping_factor,
        int                             id
)
    :   damping(damping_factor)
      , dim(similarities.rows())
      , id(id)
      , sim(similarities)
      , alpha(Eigen::MatrixXd::Zero(dim, dim))
      , rho(Eigen::MatrixXd::Zero(dim, dim))
{
    assert(sim.maxCoeff() < 0);
}

void APLayer::update(APMergeLayer const& merge)
{
    compute_rho();
    compute_alpha(merge);
}

double APLayer::score() const
{
    // Reset state storage
    double score = 0.0;
    std::vector<int> exemplar_ids;
    std::vector<int> assignments;
    assignments.reserve(dim);

    // Determine exemplars
    auto indicator = (alpha + rho).diagonal();
    for(int i=0; i<dim; ++i)
    {
        if(indicator(i) > 0.0)
        {
            exemplar_ids.push_back(i);
        }
    }

    // Return if no exemplars have been identified so far
    if(exemplar_ids.empty())
    {
        return 0.0;
    }

    // Create matrix with exemplar to data point scores
    Eigen::MatrixXd exemplar_scores(dim, exemplar_ids.size());
    for(size_t i=0; i<exemplar_ids.size(); ++i)
    {
        int id = exemplar_ids[i];
        exemplar_scores.col(i) = alpha.col(id) + rho.col(id);
    }
    // Determine exemplars for every data point
    for(int i=0; i<dim; ++i)
    {
        Eigen::MatrixXd::Index index;
        exemplar_scores.row(i).maxCoeff(&index);
        int id = exemplar_ids[index];
        assignments.push_back(id);

        if(i != id)
        {
            //score += alpha(i, id) + rho(i, id);
            score += sim(i, id);
        }
    }
    // Fix exemplar self assignments
    for(size_t i=0; i<exemplar_ids.size(); ++i)
    {
        int id = exemplar_ids[i];
        assignments[id] = exemplar_ids[i];
        //score += m_layers[0].alpha(id, id) + m_layers[0].rho(id, id);
        score += sim(id, id);
    }

    return score / dim;
}

void APLayer::compute_alpha(APMergeLayer const& merge)
{
    // Compute max message values
    Eigen::MatrixXd tau_max = merge.tau[id].cwiseMax(Eigen::MatrixXd::Zero(dim, dim));
    tau_max.diagonal() = merge.tau[id].diagonal();
    Eigen::MatrixXd rho_max = rho.cwiseMax(Eigen::MatrixXd::Zero(dim, dim));
    rho_max.diagonal() = rho.diagonal();

    // Compute message sums
    Eigen::VectorXd tau_max_sum = tau_max.colwise().sum();
    Eigen::VectorXd rho_max_sum = rho_max.colwise().sum();

    // Create new alpha matrix
    Eigen::MatrixXd new_alpha = Eigen::MatrixXd::Zero(dim, dim);
    new_alpha.rowwise() = (rho_max_sum + tau_max_sum).transpose();
    new_alpha -= rho_max;

    // Perform min opertion on non-diagonal elements
    Eigen::VectorXd new_alpha_diag(new_alpha.diagonal());
    new_alpha = new_alpha.cwiseMin(Eigen::MatrixXd::Zero(dim, dim));
    new_alpha.diagonal() = new_alpha_diag;

    // Update alpha with damping
    alpha = (damping * alpha) + ((1.0 - damping) * new_alpha);
}

void APLayer::compute_rho()
{
    // Compute values for one row at a time
    Eigen::MatrixXd tmp = Eigen::MatrixXd::Zero(dim, dim);
    for(int i=0; i<dim; ++i)
    {
        Eigen::MatrixXd::Index idx;
        Eigen::VectorXd vec = sim.row(i) + alpha.row(i);

        // Set max value for all but the max index
        double value = vec.maxCoeff(&idx);
        tmp.row(i).fill(value);
        // Set max value for the max value
        vec[idx] = -std::numeric_limits<double>::max();
        tmp(i, idx) = vec.maxCoeff();
    }

    rho = (damping * rho) + ((1.0 - damping) * (sim - tmp));
}


} /* apc */
